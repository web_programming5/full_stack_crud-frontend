export default interface Customer {
    id?: number;
    name: string;
    age: number;
    tel: string;
    gender: string;
    createdData?: Date;
    updatedDate?: Date;
    deletedDate?: Date;
  }
  