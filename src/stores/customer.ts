import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({
    name: "",
    age: 0,
    tel: "",
    gender: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedCustomer.value = { name: "", age: 0, tel: "", gender: "" };
    }
  });

  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomers();
      customers.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }
      dialog.value = false;
      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function daleteCustomer(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.deleteCustomer(id);
      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }
  return {
    customers,
    getCustomers,
    dialog,
    editedCustomer,
    saveCustomer,
    editCustomer,
    daleteCustomer,
  };
});

